Script for patching kernel configs in gentoo

## Todo
- [X] Split diff gen and patch creation as separate functions
- [ ] Add long flags
- [X] Add more file checks and edge cases
- [ ] Add functionality/flag to specify output filename (overrides default path and skips filename input)
- [ ] Add functionality/flag to send patch to stdout instead of file
- [ ] Add option to exclude patch header
- [ ] Add functionality for appending to existing patch (print existing first)
- [X] Add clean early exit function
- [ ] Work on SELECTED_MAKETARGET
- [X] Write documentation
- [ ] Improve documentation
- [X] Write ebuild

## Installation
Ebuild available as `sys-kernel/patchkernel` via personal overlay: [generalay](https://gitlab.com/pask1/generalay)
